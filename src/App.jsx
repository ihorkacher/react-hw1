import './App.module.scss';
import Button from "./components/Button/Button";
import styles from './App.module.scss';
import Modal from "./components/Modal/Modal";
import React, {useState} from "react";
import {ModalData, mainButtonsData} from "./data";
import findModalData from "./findModalData";

function App() {
    const [isOpen, setIsOpen] = useState(false);
    const [btnID, setBtnID] = useState(null);

    const closeModal = (e) => {
        setIsOpen(false);
        !isOpen && setBtnID(null);
    }
    const openModal = (e) => {
        !isOpen && setIsOpen(true);
        !isOpen && setBtnID(Number(e.target.id));
    }

    const actionsFirstModal = (<>
        <Button onClick={closeModal}>Ok</Button>
        <Button onClick={closeModal}>Cancel</Button>
    </>);
    const actionsSecondModal = (<>
        <Button onClick={closeModal}>This is the way!</Button>
    </>);

    return (
        <>
            <div className={styles.container}>
                <div className={styles.openBtnWrapper}>

                    {mainButtonsData.map(({id, backgroundColor, children}) =>
                        (
                            <React.Fragment key={id}>
                                <Button

                                    id={id}
                                    className={styles.openModalBtn}
                                    backgroundColor={backgroundColor}
                                    onClick={openModal}
                                >
                                    {children}
                                </Button>
                                {isOpen && id === btnID ?
                                    findModalData(ModalData, btnID).map(({
                                                                             headerTitle,
                                                                             isCloseButton,
                                                                             contentMessage
                                                                         }) =>
                                        (
                                            <Modal
                                                key={btnID}
                                                closeModal={closeModal}
                                                headerTitle={headerTitle}
                                                isCloseButton={isCloseButton}
                                                contentMessage={contentMessage}
                                                actions={btnID === 0 ? actionsFirstModal : btnID === 1 ? actionsSecondModal : null}
                                            />
                                        ))
                                    : null}
                            </React.Fragment>
                        )
                    )}


                </div>
            </div>
        </>
    );
}

export default App;
