
export const ModalData = [
    {
        id: 0,
        headerTitle:    <>     Do you want to delete this file?</>,
        isCloseButton: true,
        contentMessage: <>  Once you delete this file, it won't be possible to undo this action.<br/>
                            Are you sure you want to delete it?</>,
    },
    {
        id: 1,
        headerTitle:    <>     Mandalorian motto</>,
        isCloseButton: false,
        contentMessage: <>  This is the way!</>,
    },
];

export const mainButtonsData = [
    {
        id: 0,
        backgroundColor: '#0082D1',
        children: <>Open First<br/>Modal</>
    },
    {
        id: 1,
        backgroundColor: '#FFD100',
        children: <>Open Second<br/>Modal</>
    }
]
