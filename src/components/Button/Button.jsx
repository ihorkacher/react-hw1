import React from 'react';

function Button(props) {
    const {className, backgroundColor, children, onClick, id,} = props;
    return (
        <button
            id={id}
            className={className}
            style={{backgroundColor}}
            onClick={onClick}
        >{children}</button>
    );
}

export default Button;