import React from 'react';
import Button from "../Button/Button";
import styles from "./Modal.module.scss"

function Modal(props) {
    const {actions, closeModal, headerTitle, contentMessage, isCloseButton} = props;


    return (
        <>
            <div
                className={styles.background}
                onClick={closeModal}
            ></div>
            <div className={styles.modal}>
                <header className={styles.header}>
                    <div className={styles.title}>{headerTitle}</div>
                    {isCloseButton && (<Button
                        className={styles.closeButton}
                        onClick={closeModal}
                    ><>&#10006;</>
                    </Button>)}
                </header>
                <p className={styles.message}>{contentMessage}</p>
                <div className={styles.actionsWrapper}>
                    {actions}
                </div>
            </div>
        </>
    );
}

export default Modal;